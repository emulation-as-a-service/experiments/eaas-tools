FROM ubuntu:19.04

RUN mkdir /work; mkdir -p /opt/packages
WORKDIR /work

RUN apt-get -y update && apt-get -y upgrade && apt-get -y install apt-src

RUN sed -i 's/^# deb-src/deb-src/' /etc/apt/sources.list

RUN apt-src update && apt-src install qemu-block-extra
RUN sed -Ei 's/strncmp(\(\(char \*\)ptr, accept_line,)/strncasecmp\1/' qemu*/block/curl.c
RUN apt-src build qemu-block-extra

RUN mv *.deb /opt/packages
